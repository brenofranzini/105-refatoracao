package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RetanguloTest {
	Retangulo retangulo;
	List<Double> medidas;
	
	@Before
	public void preparar() {
		medidas = new ArrayList<Double>();
		medidas.add(1.0);
		medidas.add(2.0);
	}

	@Test
	public void inicializarRetanguloCorretamente() {
		retangulo = new Retangulo(medidas);
		
		Assert.assertNotNull(retangulo);
		Assert.assertNotNull(retangulo.medidas);
	}
	
	@Test
	public void calcularAAreaDoRetanguloCorretamente() {
		retangulo = new Retangulo(medidas);
		Double valor = retangulo.calcularArea();
		Double dois = 2.0;
		
		Assert.assertEquals(dois, valor);
	}
}
