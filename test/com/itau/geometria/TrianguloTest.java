package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TrianguloTest {
	Triangulo triangulo;
	List<Double> medidas;
	
	@Before
	public void preparar() {
		medidas = new ArrayList<Double>();
		medidas.add(1.0);
		medidas.add(2.0);
		medidas.add(2.0);
	}

	@Test
	public void inicializarTrianguloCorretamente() {
		triangulo = new Triangulo(medidas);
	}
	
	@Test
	public void calcularAAreaDoTrianguloCorretamente() {
		triangulo = new Triangulo(medidas);
		triangulo.calcularArea();
	}
}
