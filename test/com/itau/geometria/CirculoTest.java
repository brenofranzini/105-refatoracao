package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CirculoTest {
	Circulo circulo;
	List<Double> medidas;
	
	@Before
	public void preparar() {
		medidas = new ArrayList<Double>();
		medidas.add(1.0);
	}

	@Test
	public void inicializarCirculoCorretamente() {
		circulo = new Circulo(medidas);
		Assert.assertNotNull(circulo);
		Assert.assertNotNull(circulo.medidas);
	}
	
	@Test
	public void calcularAAreaDoCirculoCorretamente() {
		circulo = new Circulo(medidas);
		Double valor = circulo.calcularArea();
		Double pi = 3.141592653589793;
		
		Assert.assertEquals(pi, valor);
	}
}
