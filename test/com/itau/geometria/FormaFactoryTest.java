package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.itau.excecoes.FiguraNaoReconhecida;
import com.itau.excecoes.TrianguloException;

public class FormaFactoryTest {
	List<Double> medidas;

	@Before
	public void preparar() {
		medidas = new ArrayList<Double>();
	}

	@Test
	public void criarCirculo() throws Exception {
		medidas.add(1.0);
		
		new FormaFactory(medidas);
	}

	@Test
	public void criarRetangulo() throws Exception {
		medidas.add(1.0);
		medidas.add(2.0);

		new FormaFactory(medidas);
	}

	@Test
	public void criarTriangulo() throws Exception {
		medidas.add(1.0);
		medidas.add(2.0);
		medidas.add(2.0);

		new FormaFactory(medidas);
	}
	
	@Test(expected=TrianguloException.class)
	public void criarTrianguloZoado() throws Exception {
		medidas.add(1.0);
		medidas.add(2.0);
		medidas.add(1.0);

		new FormaFactory(medidas);
	}
	
	@Test(expected=FiguraNaoReconhecida.class)
	public void criarFiguraNaoReconhecida() throws Exception {
		medidas.add(1.0);
		medidas.add(2.0);
		medidas.add(2.0);
		medidas.add(4.0);

		new FormaFactory(medidas);
	}

}
