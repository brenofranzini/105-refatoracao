package com.itau.geometria;

import java.util.List;

import com.itau.excecoes.*;

public class FormaFactory {
//	public FormaFactory(List<me>)
	public FormaFactory(List<Double> medidas) throws java.lang.Exception {
		switch(medidas.size()) {
			case(1):
				System.out.println(new Circulo(medidas).calcularArea());
				break;
			case(2):
				System.out.println(new Retangulo(medidas).calcularArea());
				break;
			case(3):
				double ladoA = medidas.get(0);
				double ladoB = medidas.get(1);
				double ladoC = medidas.get(2);

				if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
					System.out.println(new Triangulo(medidas).calcularArea());
				} else {
					throw new TrianguloException();
				}
				break;
			default:
				throw new FiguraNaoReconhecida();
		}
	}
}