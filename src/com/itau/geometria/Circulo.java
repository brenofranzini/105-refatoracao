package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;

public class Circulo extends FiguraGeometrica {
	
	public Circulo(List<Double> medidas) {
		super(medidas);
	}

	protected double calcularArea() {
		return (Math.pow(medidas.get(0), 2) * Math.PI);
	}
}
