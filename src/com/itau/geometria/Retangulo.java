package com.itau.geometria;

import java.util.List;

public class Retangulo extends FiguraGeometrica{

	public Retangulo(List<Double> medidas) {
		super(medidas);
	}

	protected double calcularArea() {
		double lado = medidas.get(0);
		double largura = medidas.get(1);

		return lado * largura;
	}
}
