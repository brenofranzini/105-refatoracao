package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Console {
	
	public static void paranaue() throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		Double valor = -1.0;
		
		String entrada = new String();
		
		List<Double> lados = new ArrayList<Double>();
		
		while (true) {
			System.out.println("Digite um lado ou digite zero para encerrar: ");
			entrada = scanner.nextLine();
			valor = Double.parseDouble(entrada);

			if (valor > 0) {
				lados.add(valor);
			} else {
				break;
			}
		}
		
		scanner.close();
		
		FormaFactory formaFactory = new FormaFactory(lados);
	}
}
