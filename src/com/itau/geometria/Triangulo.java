package com.itau.geometria;

import java.util.List;

public class Triangulo extends FiguraGeometrica{

	public Triangulo(List<Double> medidas) {
		super(medidas);
	}

	protected double calcularArea() {
		double ladoA = medidas.get(0);
		double ladoB = medidas.get(1);
		double ladoC = medidas.get(2);
		
		double s = (ladoA + ladoB + ladoC) / 2;
		return Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
	}
}
