package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;

public abstract class FiguraGeometrica {

//	private int qtdLados;
	protected List<Double> medidas;

//	public FiguraGeometrica(int qtdLados, List<Double> medidas) {
	public FiguraGeometrica(List<Double> medidas) {

		this.medidas = medidas;
	}

	protected abstract double calcularArea();
}
